// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

extern crate alloc;

use super::Process;
use core::ptr::null;
use lens_system::{Result, KernelError};
use alloc::boxed::Box;

/// A template containing the parameters for launching a process.
pub struct ProcessTemplate<'a> {
    pub file: &'a str,
}

impl<'a> ProcessTemplate<'a> {
    /// Start the process.
    pub fn run(&'a self) -> Result<Process> {
        let envp: *const u8;
        envp = null();
        let mut argv: [*const u8; 2] = [null(); 2];
        let mut argv0_vec = self.file.as_bytes().to_vec();
        argv0_vec.push(0);
        argv[0] = argv0_vec[..].as_ptr();

        let pid = lens_syscalls::fork();

        if pid == 0 {
            let err: i32;
            unsafe {
                err = -(lens_syscalls::execve(
                    argv[0],
                    &argv as *const *const u8,
                    &envp as *const *const u8,
                ));
            }
            panic!("exec failed; kernel error number {}", err);
        } else if pid < 0 {
            Err(Box::new(KernelError { errno: -pid }))
        } else {
            Ok(super::Process { pid })
        }
    }

    /// Create a new process template using only the filename.
    pub fn new_basic(file: &str) -> ProcessTemplate {
        ProcessTemplate { file }
    }
}
